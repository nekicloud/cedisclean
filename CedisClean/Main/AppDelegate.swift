//
//  AppDelegate.swift
//  CedisClean
//
//  Created by Saša Vujanovic on 01/07/2019.
//  Copyright © 2019 Shauqet Cungu. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let appCoord = AppCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        appCoord.start()
        return true
    }
}

