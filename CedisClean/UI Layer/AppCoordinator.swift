//
//  AppCoordinator.swift
//  CedisClean
//
//  Created by Saša Vujanovic on 01/07/2019.
//  Copyright © 2019 Shauqet Cungu. All rights reserved.
//

import UIKit

class AppCoordinator{
    var window: UIWindow
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)){
        self.window = window
    }
    
    func start() {
        let vc = UIViewController()
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
}
